# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print  of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game

  number = rand(1..100)
  num_guesses = 1

  puts "guess a number"
  guess = gets.chomp.to_i

  until guess==number
    puts guess
    case guess < number
    when true
      puts "too low"
    when false
      puts "too high"
    end

    num_guesses += 1
    guess = gets.chomp.to_i

  end

    puts number
    puts "Good Job, it took you #{num_guesses} guess(es)."

end


# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def shuffler

  #Should add on the function of listing the files in the directory. 

  puts "Please type the name of your file(use the extension unless it's .txt): "
  file_name = gets.chomp

  case File.exists?(file_name)
    when true
      new_file = File.new("#{file_name}-shuffled.txt", "w")

      File.open(file_name, "r") do |l|
          while line = l.gets
          new_file.puts line.delete("\n").split("").shuffle.join
          end
      end
      new_file.close

    when false
      raise "Error: File does not exist. Please. try again."
  end

end
